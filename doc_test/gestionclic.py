from tkinter import *
import datetime
master=Tk()
sv = StringVar()
canvas = Canvas(master,width=700,height=250)
canvas.pack()
labepos = Label(master, textvariable=sv)
labepos.pack()
def click_callback(event):
    x1=event.x
    y1=event.y
    color='blue'
    if event.num == 1:
        color='red'
    elif event.num==3:
        color='yellow'
    canvas.create_rectangle(x1,y1,x1+20,y1+20,fill=color)
    sv.set(str(x1)+" : "+str(y1))

canvas.bind('<Button-1>',click_callback)
canvas.bind('<Button-3>',click_callback)

master.mainloop()