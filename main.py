#!/usr/bin/env python
import sys
from tkinter import *
import datetime
import random
import time
from copy import copy

param_nestx=-1
param_nesty=-1
interfaceplay=False

#Fonction pour la partie Help
def helpsection():
    print(" __      __       .__                             ._.\n/  \    /  \ ____ |  |   ____  ____   _____   ____| |\n\   \/\/   // __ \|  | _/ ___\/  _ \ /     \_/ __ \ |\n \        /\  ___/|  |_\  \__(  <_> )  Y Y  \  ___/\|\n  \__/\  /  \___  >____/\___  >____/|__|_|  /\___  >_\n       \/       \/          \/            \/     \/\/\n")
    print("python3 main.py <'Argument'>")
    print("This is the help section:")
    print("This program has default parameter but if you want to parameter this app yourself if you want!\n\n")
    print("         -cons                                                               disable the graphical part and only show in the term")
    print("         -rows='<the number of rows>'(3 by default)                          allow to input the number of rows")
    print("                 exemple: python3 main.py -rows=1000000000000000000  (min = 3 max = 45)")
    print("         -coll='<the number of coll>'(3 by default)                          allow to input the number of collumn")
    print("                 exemple: python3 main.py -coll=1000000000000000000  (min = 3 max = 45)")
    print("         -anni='<the number of time(s)>'(60(s) by default)                   allow to input the duration of the animation")
    print("                 exemple: python3 main.py -anni=1000000000000000000  (min = 15 max = 200)")
    print("         -size='<the size of a square>'(15 by default)                       allow to input the size of a an item")
    print("                 exemple: python3 main.py -size=1000000              (min = 5 max = 100)")
    print("         -wood='<the percentage of wood>'(80 by default)                     allow to input the percentage of wood in our forest (nop we are not in an AD)")
    print("                 exemple: python3 main.py -wood=80                   (min= 0 max = 100)")
    print("         -burn='<the type of propagation>'(0 by default)                     allow to input wich type of burn propagation that you want")
    print("                 exemple: python3 main.py -burn=0                   (min= 0 max = 1)")
    print("         -debug='<make the debug mode on>'(false by default)                 allow the user to activate the debug mod")

#fonction de traitement des paramétre 
def param(argv):
    param_rows = 3
    param_coll = 3
    param_anni = 60
    param_size = 15
    param_wood = 80
    param_cons = False
    param_debug=False
    param_burn=0
    for arg in argv:
        if "-" in arg:
            if "-help" in arg or "-h" in arg:
                helpsection()
                exit()
            cons=arg.find("-cons")
            rows=arg.find("-rows")
            coll=arg.find("-coll")
            anni=arg.find("-anni")
            size=arg.find("-size")
            wood=arg.find("-wood")
            burn=arg.find("-burn")
            debug=arg.find("-debug")
            if cons!=-1:
                param_cons =True
            if debug!=-1:
                param_debug =True
            if rows!=-1:
                try:
                    param_rows= int(arg[rows+6:len(arg)]) if int(arg[rows+6:len(arg)])>3 and int(arg[rows+6:len(arg)])<=45 else 3
                except:
                    param_rows=3
                    print("Error: Your rows argument isn't good")
            if coll!=-1:
                try:
                    param_coll= int(arg[coll+6:len(arg)]) if int(arg[coll+6:len(arg)])>3 and int(arg[coll+6:len(arg)]) <=45 else 3
                except:
                    param_coll=3
                    print("Error: Your coll argument isn't good")
            if anni!=-1:
                try:
                    param_anni= int(arg[anni+6:len(arg)]) if int(arg[anni+6:len(arg)]) >15 and int(arg[anni+6:len(arg)])<=200 else 60
                except:
                    param_anni=500
                    print("Error: Your row argument isn't good")
            if size!=-1:
                try:
                    param_size= int(arg[size+6:len(arg)]) if int(arg[size+6:len(arg)])>5 and int(arg[size+6:len(arg)])<=100 else 5
                except:
                    param_size=5
                    print("Error: Your size isn't good")
            if wood!=-1:
                try:
                    param_wood= int(arg[wood+6:len(arg)]) if int(arg[wood+6:len(arg)])>=0 and int(arg[wood+6:len(arg)])<=100 else 80
                except:
                    param_wood=80
                    print("Error: Your wood argument isn't good")
            if burn!=-1:
                try:
                    param_burn= int(arg[wood+6:len(arg)]) if int(arg[wood+6:len(arg)])>=0 and int(arg[wood+6:len(arg)])<=1 else 0
                except:
                    param_burn=0
                    print("Error: Your burn argument isn't good")
    print("__________                    .__                 ___________                            __   \n\______   \__ _________  ____ |__| ____    ____   \_   _____/__________   ____   _______/  |_ \n |    |  _/  |  \_  __ \/    \|  |/    \  / ___\   |    __)/  _ \_  __ \_/ __ \ /  ___/\   __\ \n |    |   \  |  /|  | \/   |  \  |   |  \/ /_/  >  |     \(  <_> )  | \/\  ___/ \___ \  |  |  \n |______  /____/ |__|  |___|  /__|___|  /\___  /   \___  / \____/|__|    \___  >____  > |__|  \n        \/                  \/        \//_____/        \/                    \/     \/        \n")
    if param_debug == True:
        print("Param_cons="+str(param_cons))
        print("Param_rows="+str(param_rows))
        print("Param_coll="+str(param_coll))
        print("Param_anni="+str(param_anni))
        print("Param_size="+str(param_size))
        print("Param_wood="+str(param_wood))
        print("Param_debug="+str(param_debug))
    return param_cons,param_rows,param_coll,param_anni,param_size,param_wood,param_debug,param_burn

def createrow(canvas,param_rows,param_coll,size):
    for i in range(param_rows):
        canvas.create_line(0,i*size,param_coll*size,i*size,fill="#476042",width=3)

def createcoll(canvas,param_rows,param_coll,size):
    for i in range(param_coll):
        canvas.create_line(i*size,0,i*size,param_rows*size,fill="#476042",width=1)

def createcube(canvas,param_rows,param_coll,size):
    for i in range(param_rows):
        for j in range(param_coll):
            canvas.create_text(i*size+(size/2),j*size+(size/2),text=str(i))
            canvas.create_rectangle(i*size,j*size,i*size+size,j*size+size,fill="white",outline="black")

def remplissage(param_wood,param_rows,param_coll,param,canvas,size,param_cons):
    global debug
    for iterationx in range(param_rows):
        for iterationy in range(param_coll):
            if random.randint(0,100)<param_wood:
                param[iterationx][iterationy] = 1
                if debug==True:
                    print(str(iterationx)+ " : " + str(iterationy) + " Is forest")
                if (param_cons)!=True:
                    canvas.create_rectangle(iterationx*size,iterationy*size,iterationx*size+size,iterationy*size+size,fill="green",outline="black")

def affi(param,param_rows,param_coll):
    print("~~~~~~~~~~~~~")
    for iterationx in range(param_rows):
        print("| ",end='')
        for iterationy in range(param_coll):
            print(str(param[iterationx][iterationy]),end=' | ')
        print()    
    print("~~~~~~~~~~~~~")

def update(param,coox,cooy,canvas,state,size,param_cons):
    if (param_cons)!=True:
        if state ==0:
            color="white"
        elif state == 1:
            color="green"
        elif state ==2:
            color="red"
        elif state ==3:
            color ="grey"
        else:
            color
        canvas.create_rectangle(coox*size,cooy*size,coox*size+size,cooy*size+size,fill=color,outline="black")
        #canvas.itemconfigure(affichage[coox][cooy],text=str(state))
    param[coox][cooy] = state

def isallblank(param,param_rows,param_coll):
    summ=0
    for iterationx in range(param_rows):
        for iterationy in range(param_coll):
            if int(param[iterationx][iterationy]) !=1:
                summ=summ+int(param[iterationx][iterationy])
    return summ

def indent(param,param_rows,param_coll,param_cons,size):
    summ=0
    for iterationx in range(param_rows):
        for iterationy in range(param_coll):
            if param[iterationx][iterationy] ==2:
                param[iterationx][iterationy]=3
                if(param_cons)!=True:
                    canvas.create_rectangle(iterationx*size,iterationy*size,iterationx*size+size,iterationy*size+size,fill="grey",outline="black")
            if param[iterationx][iterationy] ==3:
                param[iterationx][iterationy]=0
                if(param_cons)!=True:
                    canvas.create_rectangle(iterationx*size,iterationy*size,iterationx*size+size,iterationy*size+size,fill="white",outline="black")
    return summ

def gonnaburn(param,param_rows,param_coll,coox,cooy,param_burn):
    burn=0
    if (coox-1)>-1:
        if param[coox-1][cooy]==2:
            burn = burn+1
    if (coox+1)<param_rows:
        if param[coox+1][cooy]==2:
            burn = burn+1
    if (cooy-1)>-1:
        if param[coox][cooy-1]==2:
            burn = burn+1
    if (cooy+1)<param_coll:
        if param[coox][cooy+1]==2:
            burn = burn+1
    if burn > 0:
        if param_burn==1:
            if randm.randint(0,100)<(1-(1/(burn+1))*100):
                return True
        elif param_burn==0:
            return True
    return False

def transcopy(param,affichage,param_rows,param_coll):
    for iterationx in range(param_rows):
        for iterationy in range(param_coll):
            if affichage[iterationx][iterationy]==2:
                param[iterationx][iterationy]=2

def click_callback(Event):
    global param_size
    global param_nestx
    global param_nesty
    global interfaceplay
    global debug
    x1=int(Event.x/param_size)
    y1=int(Event.y/param_size)
    param_nestx=x1
    param_nesty=y1
    interfaceplay=True
    if debug == True:
        print(str(x1) + " : " + str(y1))
    doit()

def setup_tkinter(param_size,param_rows,param_coll):
    canvas_height=param_rows*param_size if param_rows*param_size<900 else 900
    canvas_width=param_coll*param_size if param_coll*param_size<900 else 900
    master =Tk()
    master.title("Burning Forest")
    master.geometry(str(canvas_width)+ "x"+str(canvas_height)) #Width x Height
    #master.resizable(width=False, height=False)        
    canvas = Canvas(master,width=canvas_width,height=canvas_height)
    canvas.pack()
    
        
    #createrow(canvas,param_rows,param_coll,param_size)
    #createcoll(canvas,param_rows,param_coll,param_size)
    createcube(canvas,param_rows,param_coll,param_size)

    canvas.grid(row=0, column=0)
    scroll_x = Scrollbar(master, orient="horizontal", command=canvas.xview)
    scroll_x.grid(row=1, column=0, sticky="ew")

    scroll_y = Scrollbar(master, orient="vertical", command=canvas.yview)
    scroll_y.grid(row=0, column=1, sticky="ns")

    canvas.configure(yscrollcommand=scroll_y.set, xscrollcommand=scroll_x.set)
    canvas.bind('<Button-1>',click_callback)
    return master,canvas


def doit():
    global param_nestx
    global param_nesty
    global master
    #param[param_nestx][param_nesty] =2
    affi(param,param_rows,param_coll)
    
    update(param,param_nestx,param_nesty,canvas,2,param_size,param_cons)
    affi(param,param_rows,param_coll)
    #affichage = copy(param)
    affichage = [[0] * param_coll for i in range(param_rows)]
    for iterationx in range(param_rows):
        for iterationy in range(param_coll):
            if debug == True:
                print(str(iterationx)+":"+str(iterationy))
            if param[iterationx][iterationy]==1 and gonnaburn(param,param_rows,param_coll,iterationx,iterationy,param_burn):
                update(affichage,iterationx,iterationy,canvas,2,param_size,param_cons)
    indent(param,param_rows,param_coll,param_cons,param_size)
    transcopy(param,affichage,param_rows,param_coll)
    affi(param,param_rows,param_coll)
    if isallblank(param,param_rows,param_coll) !=0:
        master.after(param_anni,doit)
    



if __name__ =='__main__':
    global param_cons
    global param_rows 
    global param_coll
    global param_anni 
    global param_size 
    global param_wood 
    global debug 
    global param_burn
    global master
    global canvas
    param_cons,param_rows,param_coll,param_anni,param_size,param_wood,debug,param_burn=param(sys.argv)
    affichage = [[0] * param_coll for i in range(param_rows)]
    param =[[0] * param_coll for i in range(param_rows)]
    canvas =0
    boucle = True
    param_nestx=0
    param_nesty=0
    if (param_cons)==True:
        while boucle:
            print("where did the fire nest start ?")
            param_nestx= int(input("X:"))
            param_nesty= int(input("Y:"))
            if param_nestx <=param_coll:
                if param_nesty <= param_rows:
                    param_nestx=param_nestx-1
                    param_nesty=param_nesty-1
                    boucle = False
    
    print("Different state of the boxes: \n0=Empty 1=Forest 2=Fire 3=Ash")
    
    if (param_cons)!=True:
        master,canvas=setup_tkinter(param_size,param_rows,param_coll)
        remplissage(param_wood,param_rows,param_coll,param,canvas,param_size,param_cons)

    

    if (param_cons)==True:
        remplissage(param_wood,param_rows,param_coll,param,canvas,param_size,param_cons)
        #param[param_nestx][param_nesty] =2
        affi(param,param_rows,param_coll)
        
        update(param,param_nestx,param_nesty,canvas,2,param_size,param_cons)
        affi(param,param_rows,param_coll)
        boucle = 1
        while boucle>0:
            #affichage = copy(param)
            affichage = [[0] * param_coll for i in range(param_rows)]
            for iterationx in range(param_rows):
                for iterationy in range(param_coll):
                    if debug == True:
                        print(str(iterationx)+":"+str(iterationy))
                    if param[iterationx][iterationy]==1 and gonnaburn(param,param_rows,param_coll,iterationx,iterationy,param_burn):
                        update(affichage,iterationx,iterationy,canvas,2,param_size,param_cons)
            indent(param,param_rows,param_coll,param_cons,param_size)
            transcopy(param,affichage,param_rows,param_coll)
            if isallblank(param,param_rows,param_coll) ==0:
                boucle=0
            affi(param,param_rows,param_coll)
    if (param_cons)!=True:
        master.mainloop()
        

    
                

    
    
        

